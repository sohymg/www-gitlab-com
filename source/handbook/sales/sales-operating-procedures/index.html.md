---
layout: handbook-page-toc
title: "Sales Operating Procedures"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Sales Operating Procedures

This documentation is based on the [GitLab Sales Process](https://docs.google.com/spreadsheets/d/1ISe3kb5bIbUKxo8lSYQlU-OddgfRgZgl9D0Y5CJ1iew/edit#gid=901742865) document approved in February 2020. Each phase of the sales cycle (see below) will be covered with step-by-step procedures necessary to accomplish each phase. 

1. [**Engage and Educate the Customer**](/handbook/sales/sales-operating-procedures/engage-and-educate-the-customer)
1. [**Facilitate the Opportunity**](/handbook/sales/sales-operating-procedures/facilitate-the-opportunity/)
1. **Deal Closure** (coming soon)
1. **Retain and Expand** (coming soon)
