---
layout: handbook-page-toc
title: "Design collaborator's playbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

**What's it for?**
This page acts as a quick reference of mental models for sync and async collaboration. Its purpose is to create a common language for collaboration between designers, GitLab team members and the broader GitLab community. It should be used to understand and label patterns in collaboration, pain points, where we get stuck and how to get unstuck. It is not our [design workflow](/handbook/engineering/ux/ux-designer/).

**Why is this important?**
[Collaboration](handbook/values/#collaboration) is one of GitLab's key values. At GitLab, we believe that everyone is a designer and everyone can contribute. This means the UX department is a [humble facilitator of design](handbook/engineering/ux/#were-humble-facilitators-of-user-experience-design). The design team can support collaboration across GitLab (and our broader community) by leveraging and sharing the mindsets, methods and tools of [design thinking](https://en.wikipedia.org/wiki/Design_thinking). 

## Mental models

### Divergent & convergent thinking

[Watch short video](https://www.youtube.com/watch?v=xjE2RV6IQzo)

From a young age we are often trained to get jump to solutions as quickly as possible. This prevents us from taking the time to explore fully all our options. We can overcome this challenge by understanding and leveraging divergent and convergent thinking. These are two of the foundational modes of creative problem-solving:
- Divergent thinking (go broad): Generating lots of different ideas and exploring options without too many constraints in place.
- Convergent thinking (focus in): Reflecting on your options and ideas, combining them together in unique ways, analysing them, and converging on a solution (with constaints in mind).

![img](https://pbs.twimg.com/media/Bmn3FOVCQAAt_r7.jpg)

**Actions**
- Discuss with your team whether it is time for diverging or converging
- Leverage a sync [collaborative cycle](https://www.salesforce.com/workdifferently/resources/) (scroll down to video section)
- Summarise text in a divergent thread and ask to converge on a solution ([example](https://gitlab.com/gitlab-org/gitlab-design/-/issues/817#note_335745932))

## Resources

- [Salesforce Workdifferently](https://www.salesforce.com/workdifferently/)
- [Salesforce Workdifferently: An Introduction To The 6 Principles To Work Differently (video)](https://www.salesforce.com/video/3642076/)
- [IBM enterprise design thinking](https://www.ibm.com/design/thinking/)

